/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function clearForm() {
    $(".edit-window-title").val('');
    $(".edit-window-note").val('');
    $("input[name='properties[]']").each(function () {
        $(this).prop('checked', false);
    });
}

function editForm() {
    var imgId = getEditImageId();
    var title = $(".edit-window-title").val();
    var note = $(".edit-window-note").val();
    var checkbox_array = new Array;
    var list_of_checkbox =  $("input[name='properties[]']");

    list_of_checkbox.each(function (i) {
        if ($(this).is(":checked")) {
            checkbox_array[i] = true;
        } else {
            checkbox_array[i] = false;
        }
    });

    var fileList = getListOfFiles();
    var j;
    for (j = 0; j < fileList.length; j++) {
        if (fileList[j].id === imgId) {
            fileList[j]["title"] = title;
            fileList[j]["note"] = note;
            fileList[j]["checkbox"] = checkbox_array;
            break;
        }
    }

    var checkbox_all_false = true;
    for (var i = 0; i < fileList[j].checkbox.length; i++) {
        if (fileList[j].checkbox[i] === true) {
            checkbox_all_false = false;
            break;
        }
    }

    // tiek pārbaudīts, vai jāpievieno isEdited stils pogai
    if (fileList[j].title !== "" || fileList[j].note !== "" || !checkbox_all_false) {
        $("#edit-btn-" + imgId).addClass('dropzone-img-edited');
    } else {
        $("#edit-btn-" + imgId).removeClass('dropzone-img-edited');
    }

    //myObj tiek glabāts tālāk objektu sarakstā
    setListOfFiles(fileList);

    closeEditWindow();

}

function fillForm() {
    var j;
    var imgId = getEditImageId();
    var fileList = getListOfFiles();
    for (j = 0; j < fileList.length; j++) {
        if (fileList[j].id === imgId) {
            break;
        }
    }

    if ('title' in fileList[j] && 'note' in fileList[j]){
        $(".edit-window-title").val(fileList[j].title);
        $(".edit-window-note").val(fileList[j].note);

        var list_of_checkbox =  $("input[name='properties[]']");
        list_of_checkbox.each(function (i) {
            $(this).prop('checked', fileList[j].checkbox[i]);
        });
    }
}