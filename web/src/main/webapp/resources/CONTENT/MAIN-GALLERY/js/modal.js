/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//function insertAfter(referenceNode, newNode) {
//    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
//}

var modalOpen = true;

function getModalOpen() {
    return modalOpen;
}

function myModal(display) {
    var modal = document.getElementById('myModal');

    window.onclick = function (event) {
        if (event.target === modal) {
            modalOpen = false;
            modal.style.display = "none";
        }
    };

    if (display) {
        modalOpen = true;
        modal.style.display = "block";
    } else {
        modalOpen = false;
        modal.style.display = "none";
    }
}



