/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function createUserFolder(data) {
    if (data.status === 'success') {
        name = $(".create-folder-name").val();
        if (name === "") {
            showSuccess('Albūms ir veiksmīgi izveidots!');
        } else {
            showSuccess('Albūms "' + name + '" ir veiksmīgi izveidots!');
        }
        clearModal();
        $("#main-user-content").load(document.URL + ' #main-user-content');

    }
}