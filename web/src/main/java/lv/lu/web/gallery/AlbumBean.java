/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.lu.web.gallery;

import lv.lu.web.login.UserSessionBean;
import lv.lu.piclr.database.dao.AlbumFacade;
import lv.lu.piclr.database.dao.UsersFacade;
import lv.lu.piclr.database.model.Album;
import lv.lu.piclr.database.model.Users;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import org.omnifaces.cdi.Param;

/**
 *
 * @author ebelevics
 */
@Named(value = "albumBean")
@RequestScoped
public class AlbumBean {

    @Inject @Param
    private Integer spectatedUserId;
    private String name;
    
    @Inject
    private Users currentUser;
    @Inject
    private UserSessionBean sessionBean;
    @Inject
    private AlbumFacade albumFacade;
    @Inject
    private UsersFacade usersFacade;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSpectatedUserId() {
        return spectatedUserId;
    }

    public void setSpectatedUserId(Integer spectatedUserId) {
        this.spectatedUserId = spectatedUserId;
    }

    public void create() {
        Album album = addAlbum(name);
        albumFacade.create(album);

    }

    private Album addAlbum(String name) {
        Album album = new Album();

        album.setUser(usersFacade.getReference(currentUser.getId()));
        album.setName(name);

        return album;
    }

    
    public List<Album> getAllAlbums() {
        if (spectatedUserId != null){
            return albumFacade.findMultipleUserAlbums(spectatedUserId);
        } else {
            return albumFacade.findMultipleUserAlbums(currentUser.getId());
        }
        
    }

    public List<Album> getAllAlbumsById() {
        return albumFacade.findMultipleUserAlbums(spectatedUserId);
    }

}
