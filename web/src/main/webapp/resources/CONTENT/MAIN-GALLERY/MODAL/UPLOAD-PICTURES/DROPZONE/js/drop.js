/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    //DROPZONE DROP
    $('#picture-dropzone').on('drop', function (e) {
        e.stopPropagation();
        myDrop(e);
    });

    //POST-DROPZONE DROP
    $('#upload-pictures-div').on('drop', function (e) {
        $("#dropzone-add").removeClass("dropzone-add-dragover");
        myDrop(e);
    });

});

function myDrop(e) {
    e.preventDefault();

    //TESTA NOLŪKIEM
//    console.log(e.originalEvent.dataTransfer.files);
    var files = e.originalEvent.dataTransfer.files;

    for (var i = 0; i < files.length; i++) {
        if (fileIsValid(files[i])) {
            updateFiles(files[i]);
        }
    }
}

