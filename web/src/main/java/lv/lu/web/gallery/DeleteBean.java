/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.lu.web.gallery;

import lv.lu.piclr.database.dao.AlbumFacade;
import lv.lu.piclr.database.dao.PicturesFacade;
import lv.lu.piclr.database.dao.UsersFacade;
import lv.lu.piclr.database.model.Album;
import lv.lu.piclr.database.model.Pictures;
import lv.lu.piclr.database.model.Users;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

/**
 *
 * @author ebelevics
 */
@Named(value = "deleteBean")
@RequestScoped
public class DeleteBean {

    @Inject
    private PicturesFacade pictureFacade;

    @Inject
    private AlbumFacade albumFacade;

    @Inject
    private UsersFacade usersFacade;

    public void deletePicture(Pictures picture) {
        System.out.println(picture.getId());
        pictureFacade.remove(picture);

    }

//    @Transactional
    public String deleteAlbum(Album album) {
        List<Pictures> albumPictureList = pictureFacade.findAllAlbumPictures(album.getId());
        if (albumPictureList != null) {
            for (Pictures picture : albumPictureList) {
                pictureFacade.remove(picture);
            }
        }

        System.out.println(album);
        albumFacade.remove(album);
        //albumFacade.removeByKey(album.getId());

        return "success";

    }

    public void deleteAll() {
        List<Pictures> allPictures = pictureFacade.findAll();
        for (Pictures currentPicture : allPictures) {
            pictureFacade.remove(currentPicture);
        }

        List<Album> allAlbums = albumFacade.findAll();
        for (Album currentAlbum : allAlbums) {
            albumFacade.remove(currentAlbum);
        }

        List<Users> allUsers = usersFacade.findAll();
        for (Users currentUser : allUsers) {
            usersFacade.remove(currentUser);
        }

    }

}
