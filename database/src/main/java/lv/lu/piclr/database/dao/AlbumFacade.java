/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.lu.piclr.database.dao;

import lv.lu.piclr.database.model.Album;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author ebelevics
 */
@Stateless
public class AlbumFacade extends AbstractFacade<Album> {

    @PersistenceContext(unitName = "piclrPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AlbumFacade() {
        super(Album.class);
    }

    /*Tiek atlasīti albūmi, kur userId sakrīt ar paramentru
    JOIN FETCH savieno album un referencēto user Query vienā SELECT Query
    */
    public List<Album> findMultipleUserAlbums(Integer userId) {
        TypedQuery<Album> query = getEntityManager().createQuery("SELECT a FROM Album a JOIN FETCH a.user WHERE a.user.id = :userId", Album.class);
        query.setParameter("userId", userId);
        List<Album> userAlbumList = query.getResultList();
        if (userAlbumList.isEmpty()) {
            return null;
        } else {
            return userAlbumList;
        }
    }

}
