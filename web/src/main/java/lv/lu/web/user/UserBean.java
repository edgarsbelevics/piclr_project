/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.lu.web.user;

import lv.lu.piclr.database.dao.UsersFacade;
import lv.lu.piclr.database.model.Users;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 *
 * @author ebelevics
 */
@Named(value = "userBean")
@RequestScoped
public class UserBean {
    private static final String CURRENT_USER_KEY = "currentUser"; 

    @Inject
    private UsersFacade usersFacade;

    private Users currentUser;

    public boolean userIdMatch(Users user){
        return currentUser.getId().equals(user.getId());
    }

    public Users getUser() {
        return currentUser;
    }

    @Produces
    @RequestScoped
    public Users getCurrentUser() {
        FacesContext context = FacesContext.getCurrentInstance();
        Integer userId = (Integer) context.getExternalContext().getSessionMap().get(CURRENT_USER_KEY);
        currentUser = usersFacade.find(userId);
        return currentUser;
    }
}
