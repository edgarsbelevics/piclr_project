/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $('#picture-dropzone').on('dragover', function (e) {
        $(this).addClass("dropzone-dragover");
        return false;
    });

    $('#picture-dropzone').on('dragleave', function (e) {
        $(this).removeClass("dropzone-dragover");
        return false;
    });

    $('#upload-pictures-div').on('dragover', function (e) {
        $('#dropzone-add').addClass("dropzone-add-dragover");
        return false;
    });

    $('#upload-pictures-div').on('dragleave', function (e) {
        $('#dropzone-add').removeClass("dropzone-add-dragover");
        return false;
    });


});

