/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.lu.web.login;

import lv.lu.piclr.database.dao.UsersFacade;
import lv.lu.piclr.database.model.Users;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author ebelevics
 */
@RequestScoped
@Named(value = "userSessionBean")
public class UserSessionBean {

    @Inject
    private UsersFacade usersFacade;
    
    private static final String CURRENT_USER_KEY = "currentUser"; 

    //iegūst useri no sesijas
    public Users getCurrentUser() {
        FacesContext context = FacesContext.getCurrentInstance();
        Integer userId = (Integer) context.getExternalContext().getSessionMap().get(CURRENT_USER_KEY);
        return usersFacade.find(userId);
    }

    //pievieno useri no sesijai
    public void setCurrentUser(Integer currentUserId) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getSessionMap().put(CURRENT_USER_KEY, currentUserId);
    }

    //izņem useri no sesijas
    public void removeCurrentUser() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().invalidateSession();
    }
    
}
