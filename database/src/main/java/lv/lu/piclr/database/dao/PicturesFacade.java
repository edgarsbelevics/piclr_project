/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.lu.piclr.database.dao;

import lv.lu.piclr.database.model.Pictures;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author ebelevics
 */
@Stateless
public class PicturesFacade extends AbstractFacade<Pictures> {

    @PersistenceContext(unitName = "piclrPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PicturesFacade() {
        super(Pictures.class);
    }

    public List<Pictures> findMultipleUserPictures(Integer userId, Integer albumId) {
        TypedQuery<Pictures> query;

        if (albumId == null) {
            query = getEntityManager().createQuery("SELECT p FROM Pictures p JOIN FETCH p.user WHERE p.user.id = :userId AND p.album.id IS NULL", Pictures.class);
        } else {
            query = getEntityManager().createQuery("SELECT p FROM Pictures p JOIN FETCH p.user WHERE p.user.id = :userId AND p.album.id = :albumId", Pictures.class);
            query.setParameter("albumId", albumId);
        }
        query.setParameter("userId", userId);
        List<Pictures> userPicturesList = query.getResultList();
        if (userPicturesList.isEmpty()) {
            return null;
        } else {
            return userPicturesList;
        }
    }

    public List<Pictures> findAllAlbumPictures(Integer albumId) {
        TypedQuery<Pictures> query;

            query = getEntityManager().createQuery("SELECT p FROM Pictures p WHERE p.album.id = :albumId", Pictures.class);

        query.setParameter("albumId", albumId);
        List<Pictures> albumPicturesList = query.getResultList();
        if (albumPicturesList.isEmpty()) {
            return null;
        } else {
            return albumPicturesList;
        }
    }

}
