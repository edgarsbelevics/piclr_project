/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.lu.web;

import lv.lu.piclr.database.dao.UsersFacade;
import lv.lu.piclr.database.model.Users;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

/**
 *
 * @author ebelevics
 */
@Named(value = "searchBean")
@RequestScoped
public class SearchBean {

    private Integer userId;
    private String searchText;

    @Inject
    private UsersFacade usersFacade;

    private List<Users> foundUsers;

    public String searchUsers() {
        return "search?search=" + searchText + "&faces-redirect=true";
    }

    public boolean isUserProfilePicture() {
        Users currentUser = usersFacade.find(userId);
        if (currentUser.getProfilePicture() == null) {
            return false;
        } else {
            return true;
        }
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public Integer getUserId() {
        System.out.println("GETTING");
        System.out.println(userId);
        return userId;
    }

    public void setUserId(Integer userId) {
        System.out.println("SETTING");
        System.out.println(userId);
        this.userId = userId;
    }

    public List<Users> getFoundUsers() {
        foundUsers = usersFacade.searchByString(searchText);
        return foundUsers;
    }

    public void setFoundUsers(List<Users> foundUsers) {
        this.foundUsers = foundUsers;
    }

}
