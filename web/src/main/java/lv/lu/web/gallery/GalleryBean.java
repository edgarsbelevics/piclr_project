/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.lu.web.gallery;

import lv.lu.web.login.UserSessionBean;
import lv.lu.piclr.database.dao.PicturesFacade;
import lv.lu.piclr.database.model.Pictures;
import lv.lu.piclr.database.model.Users;
import java.io.Serializable;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import org.omnifaces.cdi.Param;
import org.omnifaces.cdi.ViewScoped;

/**
 *
 * @author ebelevics
 */
@Named(value = "galleryBean")
@RequestScoped
public class GalleryBean {

    @Inject @Param
    private Integer spectatedUserId;
    private List<Pictures> pictures;
    @Inject @Param
    private Integer albumId;

    @Inject
    private Users currentUser;
    @Inject
    private UserSessionBean sessionBean;
    @Inject
    private PicturesFacade picturesFacade;

    public List<Pictures> getPictures() {
        if (spectatedUserId != null){
            return picturesFacade.findMultipleUserPictures(spectatedUserId, albumId);
        } else {
            return picturesFacade.findMultipleUserPictures(currentUser.getId(), albumId);
        }
        
    }

    public void setPictures(List<Pictures> pictures) {
        this.pictures = pictures;
    }

    public Integer getAlbumId() {
        return albumId;
    }

    public void setAlbumId(Integer albumId) {
        this.albumId = albumId;
    }

    public Integer getSpectatedUserId() {
        return spectatedUserId;
    }

    public void setSpectatedUserId(Integer spectatedUserId) {
        this.spectatedUserId = spectatedUserId;
    }

}
