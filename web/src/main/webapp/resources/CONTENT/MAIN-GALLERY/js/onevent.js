/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function removeUserImage(data, title) {
    if (data.status === 'success') {
        if (title === "") {
            showSuccess('Bilde ir veiksmīgi izdzēsta!');
        } else {
            showSuccess('"' + title + '" ir veiksmīgi izdzēsta!');
        }
        
        $("#main-user-content").load(document.URL + ' #main-user-content');
    }
}

function removeUserAlbum(data, name) {
    if (data.status === 'success') {
        if (name === "") {
            showSuccess('Albūms ir veiksmīgi izdzēsts!');
        } else {
            showSuccess('Albūms "' + name + '" ir veiksmīgi izdzēsts!');
        }
        $("#main-user-content").load(document.URL + ' #main-user-content');
    }
}