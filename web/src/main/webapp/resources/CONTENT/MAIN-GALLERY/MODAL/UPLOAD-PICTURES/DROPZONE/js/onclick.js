/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//uzspiež uz input
function onClickDropzone() {
    document.getElementById("imageInput").click();
}

function resetOnChange(input) {
    input.value = "";
}

//onChange pilda paslēpšanas darbības
function onChangeDropzone(e) {
    e.stopPropagation();
    myClick(e);
}

function myClick(e) {
    var files = e.target.files;
    for (var i = 0; i < files.length; i++) {
        //pārbauda, vai fails ir derīgs
        if (fileIsValid(files[i])){
            updateFiles(files[i]);
        }
    }
}