/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.lu.web.login;

import lv.lu.piclr.database.dao.UsersFacade;
import lv.lu.piclr.database.model.Users;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

/**
 *
 * @author ebelevics
 */
@Named(value = "registerBean")
@RequestScoped
public class RegisterBean {

    @Inject
    private UsersFacade usersFacade;

    private String firstname;
    private String lastname;
    private String username;
    private String email;
    private String userPassword;
    private String userPassword2;

    /*Tiek pievienots lietotājs DB*/
    public String register() {
        Users user;
        user = usersFacade.findByUsername(username);
        if (user != null) {
            return "pretty:register";
        }
        user = usersFacade.findByEmail(email);
        if (user != null) {
            return "pretty:register";
        }

        if (username == null || username.isEmpty()) {
            return "pretty:register";
        }
        if (email == null || email.isEmpty()) {
            return "pretty:register";
        }
        if ((userPassword == null) || !(userPassword.equals(userPassword2))) {
            return "pretty:register";
        }

        user = addUser(firstname, lastname, username, email, userPassword);
        usersFacade.create(user);

        return "pretty:login";

    }

    /*Tiek sagatavots jauns objekts lietotāja pievienošanai*/
    private Users addUser(String firstname, String lastname, String username, String email, String userPassword) {
        Users user = new Users();

        user.setFirstname(firstname);
        user.setLastname(lastname);
        user.setUsername(username);
        user.setEmail(email);
        user.setUserPassword(userPassword);

        return user;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserPassword2() {
        return userPassword2;
    }

    public void setUserPassword2(String userPassword2) {
        this.userPassword2 = userPassword2;
    }

}
