//tiek izmantots, lai paziņotu par veiksmīgi ielādētu bildi
// 0 -> nedod ziņu, 1 -> bildes msg, 2 -> bilžu msg
var imgSuccess = 0;
//tiek izmantots, lai padotu jaunam img-div id
var imgId = 0;
//editFormas logs pēc defaulta aizvērts
var editOpen = false;
var editImageId = 0;
//list of img-div id's (kopējais bilžu sk)
var listOfImgId = [];
var listOfFiles = [];

function getImgSuccess() {
    return imgSuccess;
}
function setImgSuccess(int) {
    imgSuccess = int;
}

function getListOfFiles() {
    return listOfFiles;
}
function setListOfFiles(list) {
    listOfFiles = list;
}

function getEditImageId() {
    return editImageId;
}
function clearListOfImgId() {
    for (var i = 0; i < listOfImgId.length; ) {
        removeImage(listOfImgId[i]);
    }
}

function updateFiles(file) {

    //ja bilžu ielāde ir veiksmīga, div sagatavo atbilstoši stilam
    document.getElementById("picture-dropzone").className = 'dropzone-hide';
    $("#dropzone-add").css('display', 'inline-block');
    $(".dropzone-upload-btns").show();

    imgId++;

    jQuery('<div/>', {
        id: 'loader-' + imgId,
        class: 'piclr-loader'
    }).insertBefore("#dropzone-add");



    var img_Id = imgId;

    var reader = new FileReader();
    reader.addEventListener("load", function () {
        imageIsLoaded(img_Id, reader);
        //Internet Explorer šī remove() metode nestrādā, taču Edge strādā
        document.getElementById('loader-' + img_Id).remove();
    });
    reader.readAsDataURL(file);


    //ielieku imdId masīvā
    listOfImgId.push(imgId);

    //tiek izveidots JSON checkbox properties uzreiz, lai nerastos kļūdas pārsūtot uz Java
    //ieteicamais uzlabojums, izveidot pilnu JSON formu faila veidošanas sākumā

    var checkbox_array = new Array;
//var list_of_checkbox = $(".edit-window-properties").find("input");
    var list_of_checkbox = $("input[name='properties[]']");

    list_of_checkbox.each(function (i) {
        checkbox_array[i] = false;
    });

    //izveidoju myFile objektu ar attiecīgo img index un failu
    myFile = {"id": imgId, "data": file, "checkbox": checkbox_array};
    //un to ievietoju sarakstā, ņemot vērā preview esošo bilžu skaitu
    listOfFiles[listOfImgId.length - 1] = myFile;
}


function imageIsLoaded(img_Id, reader) {

    var currentImgDiv = $("#image-div").clone().prop({id: "image-div-" + img_Id}).insertAfter('#loader-' + img_Id);
    currentImgDiv.find('.dropzone-img').prop({id: "image-" + img_Id, src: reader.result});
    currentImgDiv.find('.dropzone-img-rem').prop({id: "rem-btn-" + img_Id});
    currentImgDiv.find('.dropzone-img-edit').prop({id: "edit-btn-" + img_Id});
    currentImgDiv.show();

}

function removeImageByThis(ele) {
    var str_id = ele.id.replace(/rem-btn-/, '');
    var id = (parseInt(str_id));
    removeImage(id);
}

function removeImage(id) {

    document.getElementById('image-div-' + id).remove();

    var index = listOfImgId.indexOf(id);
    // izņems no masīva 1 elementu ar attiecīgo index
    listOfImgId.splice(index, 1);

    // jāizdzēž attiecīgais fails no failu saraksta
    for (var i = 0; i < listOfFiles.length; i++) {
        if (listOfFiles[i].id === id) {
            listOfFiles.splice(i, 1);
            break;
        }
    }


    if (listOfImgId.length === 0) {
        document.getElementById("picture-dropzone").className = 'dropzone';
        $("#dropzone-add").hide();
        $(".dropzone-upload-btns").hide();
        imgId = 0;
    }
}

function closeEditWindow() {
    $('#edit-window').css('display', 'none');
    editOpen = false;
    clearForm();
}

function editImage(e, ele) {
    var str_id = ele.id.replace(/edit-btn-/, '');
    var id = (parseInt(str_id));
    var position = $("#image-div-" + id).position();
    //ja edit logs ir paslēpts
    if ($('#edit-window').css('display') === 'none')
    {
        if (!editOpen) {
            editOpen = true;
            editImageId = id;
            fillForm();
            e.stopPropagation();
        }
        $('#edit-window').css({top: position.top + 10, left: position.left + 160});
        $('#edit-window').css('display', 'block');
    } else {
        closeEditWindow();
    }

}

$("#myModal").click(function (e)
{
    var container = $("#edit-window");
    if (editOpen) {
        if (!container.is(e.target) // ja target nav conteineris
                && container.has(e.target).length === 0) // ... vai conteinera komponentes
        {
            closeEditWindow();
        }
    }
});

function uploadPicutures(currentUserId, albumId) {
    if (albumId === null) {
        console.log("without album");
    } else {
        console.log("with album");
    }

    console.log("Current user = " + currentUserId);
    console.log("AlbumId = " + albumId);
    console.log(listOfFiles);

    var formData = new FormData();
//    var xhr = new XMLHttpRequest();

    for (var i = 0; i < listOfFiles.length; i++) {
        formData.append("files", listOfFiles[i].data);
    }

    formData.append("currUserId", currentUserId);
    formData.append("albumId", albumId);

    for (var i = 0; i < listOfFiles.length; i++) {

        formData.append("checkbox", listOfFiles[i].checkbox);

        if ('title' in listOfFiles[i] && 'note' in listOfFiles[i]) {
            formData.append("title", listOfFiles[i].title);
            formData.append("note", listOfFiles[i].note);
        } else {
            formData.append("title", "");
            formData.append("note", "");
        }
    }

    $.ajax({
        url: "UploadServlet",
        data: formData,
        processData: false,
        contentType: false,
        type: 'POST',

        success: function () {
            $("#main-user-content").load(document.URL + ' #main-user-content');
            console.log(listOfFiles.length);
            if (listOfFiles.length > 1) {
                showSuccess("Bildes ir veiksmīgi augšuplādētas!");
            } else {
                showSuccess("Bilde ir veiksmīgi augšuplādēta!");
            }
            clearModal();
        },
        error: function () {
            showAlert("Parādījās kļūda augšuplādējot bildi!");
            clearModal();
        }

    });
}