/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.lu.web.login;

import lv.lu.piclr.database.dao.UsersFacade;
import lv.lu.piclr.database.model.Users;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

/**
 *
 * @author ebelevics
 */
@Named(value = "loginBean")
@RequestScoped
public class LoginBean {

    @Inject
    private UserSessionBean sessionBean;

    @Inject
    private UsersFacade usersFacade;

    private String email_username;
    private String userPassword;

    /*Tiek sagatavots Lietotājs sesijai*/
    public String login() {
        Users user = getUser();
        if (user != null) {
            sessionBean.setCurrentUser(user.getId());
            return "pretty:main_gallery";
        }
        return null;
    }
    

    /*Tiek veikta autentifikācija*/
    public Users getUser() {

        Users user = usersFacade.findByEmail(getEmail_username());
        if (user == null) {
            user = usersFacade.findByUsername(getEmail_username());
        }

        if (user != null) {

            if (user.getUserPassword().equals(getUserPassword())) {
                return user;
            }
        }

        return null;
    }

    public String logout() {
        sessionBean.removeCurrentUser();
        return "pretty:login";

    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getEmail_username() {
        return email_username;
    }

    public void setEmail_username(String email_username) {
        this.email_username = email_username;
    }

}
