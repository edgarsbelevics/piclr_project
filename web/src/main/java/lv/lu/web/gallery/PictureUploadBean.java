package lv.lu.web.gallery;

import lv.lu.web.login.UserSessionBean;
import lv.lu.piclr.database.dao.AlbumFacade;
import lv.lu.piclr.database.dao.PicturesFacade;
import lv.lu.piclr.database.dao.UsersFacade;
import lv.lu.piclr.database.model.Pictures;
import lv.lu.piclr.database.model.Users;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.Part;
import org.apache.commons.io.IOUtils;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author ebelevics
 */
@Named(value = "pictureUploadBean")
@RequestScoped
public class PictureUploadBean implements Serializable {

    private UploadedFile profilePicture;
    private byte[] picture;
    private String title;
    private String note;
    private String[] properties;
    private boolean _public;
    private boolean commentable;
    private boolean rateable;
    private boolean piclr;
    private boolean downloadable;

    @Inject
    private UserSessionBean sessionBean;

    @Inject
    private UsersFacade usersFacade;

    @Inject
    private PicturesFacade pictureFacade;

    @Inject
    private AlbumFacade albumFacade;

    public void upload(Integer currUserId, Integer albumId, List<Part> files, String[] titles, String[] notes, String[] checkboxes) throws IOException {
        for (int i = 0; i < files.size(); i++) {

            InputStream input = files.get(i).getInputStream();
            picture = IOUtils.toByteArray(input);

            String[] propertyArray = checkboxes[i].split(",");

            Users uploader = usersFacade.find(currUserId);
            Pictures addedPicture = addPicture(uploader, picture, titles[i], notes[i], propertyArray);

            if (albumId != null) {
                addedPicture.setAlbum(albumFacade.find(albumId));
            }

            pictureFacade.create(addedPicture);
        }
    }

    private Pictures addPicture(Users uploader, byte[] picture, String title, String note, String[] properties) {
        Pictures addedPicture = new Pictures();

        // lai nerastos boolean pārklājumi ar citām iepriekšējām bildēm
        setPublic(false);
        setCommentable(false);
        setRateable(false);
        setPiclr(false);
        setDownloadable(false);

        for (int i = 0; i < properties.length; i++) {
            if ("true".equals(properties[i])) {
                switch (i) {
                    case 0:
                        setPublic(true);
                        break;
                    case 1:
                        setCommentable(true);
                        break;
                    case 2:
                        setRateable(true);
                        break;
                    case 3:
                        setPiclr(true);
                        break;
                    case 4:
                        setDownloadable(true);
                        break;
                }
            }

        }

        addedPicture.setUser(uploader);
        addedPicture.setPicture(picture);
        addedPicture.setTitle(title);
        addedPicture.setNote(note);
        addedPicture.setPublic(_public);
        addedPicture.setCommentable(commentable);
        addedPicture.setRateable(rateable);
        addedPicture.setPiclr(piclr);
        addedPicture.setDownloadable(downloadable);
        return addedPicture;
    }

    public void profileUpload() {
        Users currentUser = sessionBean.getCurrentUser();

        String type = profilePicture.getContentType().split("/")[0];
        if (type.equals("image")) {
            currentUser.setProfilePicture(profilePicture.getContents());
        }

        usersFacade.edit(currentUser);
    }

    public void profileRemove() {
        Users currentUser = sessionBean.getCurrentUser();
        currentUser.setProfilePicture(null);
        usersFacade.edit(currentUser);
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String[] getProperties() {
        return properties;
    }

    public void setProperties(String[] properties) {
        this.properties = properties;
    }

    public boolean isPublic() {
        return _public;
    }

    public void setPublic(boolean _public) {
        this._public = _public;
    }

    public boolean isCommentable() {
        return commentable;
    }

    public void setCommentable(boolean commentable) {
        this.commentable = commentable;
    }

    public boolean isRateable() {
        return rateable;
    }

    public void setRateable(boolean rateable) {
        this.rateable = rateable;
    }

    public boolean isPiclr() {
        return piclr;
    }

    public void setPiclr(boolean piclr) {
        this.piclr = piclr;
    }

    public boolean isDownloadable() {
        return downloadable;
    }

    public void setDownloadable(boolean downloadable) {
        this.downloadable = downloadable;
    }

    public UploadedFile getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(UploadedFile profilePicture) {
        this.profilePicture = profilePicture;
    }

}
