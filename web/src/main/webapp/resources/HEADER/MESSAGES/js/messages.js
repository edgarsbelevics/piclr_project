/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {

});

function showAlert(text) {
    $(".message").hide();
    $(".message-txt").text(text);
    $(".message.alert").fadeIn(200);
    closeMessageAfterTime();
}
function showSuccess(text) {
    $(".message").hide();
    $(".message-txt").text(text);
    $(".message.success").fadeIn(200);
    closeMessageAfterTime();
}
function showInfo(text) {
    $(".message").hide();
    $(".message-txt").text(text);
    $(".message.info").fadeIn(200);
    closeMessageAfterTime();
}
function showWarning(text) {
    $(".message").hide();
    $(".message-txt").text(text);
    $(".message.warning").fadeIn(200);
    closeMessageAfterTime();
}

function closeMessageAfterTime() {
    setTimeout(function () {
        $(".message").fadeOut(500);
    }, 10000);
}

function closeMessage() {
    $(".message").fadeOut(500);
}