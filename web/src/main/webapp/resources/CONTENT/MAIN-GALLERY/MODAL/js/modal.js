/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function showUploadPictures() {
    document.getElementById("upload-pic-btn").className = 'piclr-active';
    document.getElementById("create-folder-btn").className = 'piclr-hover';

    document.getElementById("create-album-div").className = 'piclr-hide';
    document.getElementById("upload-pictures-div").className = 'piclr-show';

}

function showCreateAlbum() {
    document.getElementById("upload-pic-btn").className = 'piclr-hover';
    document.getElementById("create-folder-btn").className = 'piclr-active';

    document.getElementById("upload-pictures-div").className = 'piclr-hide';
    document.getElementById("create-album-div").className = 'piclr-show';

}


