/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.lu.web;

import lv.lu.web.gallery.PictureUploadBean;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author ebelevics
 */
@MultipartConfig
@WebServlet(name = "UploadServlet", urlPatterns = {"/UploadServlet"})
public class UploadServlet extends HttpServlet {

    @Inject
    private PictureUploadBean pictureUpload;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Part> files = (List<Part>) request.getParts();

        for (int i = 0; i < files.size(); i++) {
            String type;

            try {
                type = files.get(i).getContentType().split("/")[0];
                if (type.equals("image")) {
//                    System.out.println(parts.get(i).getContentType());

                } else {
                    files.remove(i);
                    i--;
                }

            } catch (Exception e) {
                //exceptions būs no non-Part objektiem
                files.remove(i);
                i--;
            }

        }
        
        Integer albumId = null;
        if (!("null".equals(request.getParameter("albumId")))) {
            albumId = Integer.parseInt(request.getParameter("albumId"));
            
        }
//        System.out.println(albumId);
        
        Integer currUserId = Integer.parseInt(request.getParameter("currUserId"));

        String[] checkboxes = request.getParameterValues("checkbox");
        String[] titles = request.getParameterValues("title");
        String[] notes = request.getParameterValues("note");

//          checkboxes = checkboxes[0].split(" ");
//
//        System.err.println(Arrays.toString(titles));
//        System.err.println(Arrays.toString(notes));
//        System.err.println(Arrays.toString(checkboxes));
//        System.err.println(files);
        pictureUpload.upload(currUserId, albumId, files, titles, notes, checkboxes);

    }

}
