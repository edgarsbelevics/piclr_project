/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.lu.web.gallery;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author ebelevics
 */
@Named(value = "forgotPasswordBean")
@RequestScoped
public class ForgotPasswordBean {

    private String email_username;


    /* TODO password reset */
    public String resetPassword() {
        return "login";
    }


    public String getEmail_username() {
        return email_username;
    }

    public void setEmail_username(String email_username) {
        this.email_username = email_username;
    }

}
