/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.lu.piclr.database.dao;

import lv.lu.piclr.database.model.Users;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author ebelevics
 */
@Stateless
public class UsersFacade extends AbstractFacade<Users> {

    @PersistenceContext(unitName = "piclrPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsersFacade() {
        super(Users.class);
    }

    public Users findByEmail(String email) {
        TypedQuery<Users> query = getEntityManager().createQuery("SELECT u FROM Users u WHERE u.email = :email", Users.class);
        query.setParameter("email", email);
        List<Users> userList = query.getResultList();
        if (userList.isEmpty()) {
            return null;
        } else {
            return userList.get(0);
        }
    }

    public Users findByUsername(String username) {
        TypedQuery<Users> query = getEntityManager().createQuery("SELECT u FROM Users u WHERE u.username = :username", Users.class);
        query.setParameter("username", username);
        List<Users> userList = query.getResultList();
        if (userList.isEmpty()) {
            return null;
        } else {
            return userList.get(0);
        }
    }

    public List<Users> searchByString(String search) {
        TypedQuery<Users> query = getEntityManager().createQuery("SELECT u FROM Users u WHERE u.username LIKE :search OR u.firstname LIKE :search OR u.lastname LIKE :search", Users.class);
        query.setParameter("search", "%" + search + "%");
        List<Users> userList = query.getResultList();
        if (userList.isEmpty()) {
            return null;
        } else {
            return userList;
        }
    }
}
