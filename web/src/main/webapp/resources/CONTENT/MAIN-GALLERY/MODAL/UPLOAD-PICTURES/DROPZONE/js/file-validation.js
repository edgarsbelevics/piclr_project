/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function fileIsValid(file) {
    if (!fileTypeValidation(file)) {
        return false;
    }
    if (!fileSizeValidation(file)) {
        return false;
    }
    return true;
}


function fileTypeValidation(file) {
    if (file.type === "image/png" || file.type === "image/gif" || file.type === "image/jpeg") {
        return true;
    } else {
        showWarning('"'+ file.name + '" nav attēla formātā!');
        return false;
    }
}

function fileSizeValidation(file) {
    // jo MEDIUM BLOB izmērs ir ap 16MB
    if (file.size >= 10000000) {
        showWarning('"'+ file.name + '" pārsniedz atļautos 10 MB!');
        return false;
    } else {
        return true;
    }
}

